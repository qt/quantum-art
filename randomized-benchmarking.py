# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.6.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
import qiskit
import json
from qiskit.tools.monitor import job_monitor
import qiskit.ignis.verification.randomized_benchmarking as rb
from qiskit import IBMQ
from pathlib import Path
from datetime import datetime
import pickle


def prepare_data(folder, backend_name='ibmq_ourense', shots=8000):
    if shots > 8000:
        raise ValueError('Number of shots must be smaller than 8000.')

    base = Path(folder)
    (base / 'circuit_figs').mkdir(parents=True, exist_ok=True)

    # Authentication
    # Follow instructions in https://qiskit.org/documentation/install.html#access-ibm-quantum-systems
    IBMQ.load_account()
    IBMQ.providers()
    provider = IBMQ.get_provider(group='open')
    backend = provider.get_backend(backend_name)

    #Generate RB circuits (1Q RB)
    print('Generating circuits')
    rb_opts = {}
    #Number of Cliffords in the sequence
    lengths = np.arange(1, 500, 500//70)
    #Number of seeds (random sequences)
    rb_opts['nseeds'] = 1
    rb_opts['rb_pattern'] = [[0]] if len(backend.properties().qubits) == 1 else [[0, 1]]

    rb_circs = [[]]
    for length in lengths:
        rb_opts['length_vector'] = [length]
        circ = rb.randomized_benchmarking_seq(**rb_opts)[0][0][0]
        rb_circs[0].append(circ)

    with open(base / 'circuits.pkl', 'wb') as f:
        pickle.dump(rb_circs[0], f)

    # Transpile circuits
    print('Transpiling circuits')
    basis_gates = ['u1', 'u2', 'u3', 'cx']
    new_rb_circ = qiskit.compiler.transpile(rb_circs[0], basis_gates=basis_gates)
    transpiled_circs = new_rb_circ

    # Execute circuits
    print('Running circuits')
    job = qiskit.execute(new_rb_circ, backend=backend, shots=shots, memory=True)
    job_monitor(job)
    results = job.result().results

    # Write noise pattern
    def hex_to_bin(hex_num):
        bin_num = bin(int(hex_num, base=16))[2:]
        if len(bin_num) == 1:
            bin_num = '0' + bin_num
        return bin_num

    shots = [[hex_to_bin(measurement) for measurement in res.data.memory] for res in results]
    shots = ''.join(str(i) for i in np.ravel(shots))
    (base / 'quantum_noise.txt').write_text(shots)
    (base / 'quantum_noise_0.txt').write_text(shots[::2])
    (base / 'quantum_noise_1.txt').write_text(shots[1::2])

    # Write job info
    job_info = dict(backend_name=job.backend().name(),
                backend_version=job.backend().version(),
                qobj_id=job.qobj().qobj_id,
                job_id=job.job_id())
    (base / 'job_info.json').write_text(json.dumps(job_info))

    return rb_circs[0], results


folder = str(datetime.now().timestamp())
circs, results = prepare_data(folder)
