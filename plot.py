# -*- coding: utf-8 -*-
import qiskit
from pathlib import Path
import pickle
from copy import deepcopy
import numpy as np


def plot_circuits(folder, circs, fold=60, aspect_ratio=None):
    base = Path(folder)
    (base / 'circuit_figs').mkdir(parents=True, exist_ok=True)

    # Plot circuits
    for i, circ in enumerate(circs):
        circ = deepcopy(circ)
        circ.cregs = []
        circ.data = circ.data[:len(circ.data) // 2]
        circ.data = [gate for gate in circ.data if not isinstance(gate[0], qiskit.circuit.barrier.Barrier)]
        if aspect_ratio:
            scaling = 41.625
            fold = np.sqrt(aspect_ratio * len(circ.data) * scaling)
        plot = str(circ.draw('text', fold=fold))
        plot = plot.replace("«", '').replace('»', '')
        (base / f'circuit_figs/circuit_{i}.txt').write_text(plot)


folder = '1599599247.682351'
with open(Path(folder) / 'circuits.pkl', 'rb') as f:
    # Plot with fixed number of characters
    plot_circuits(folder, pickle.load(f), fold=200)
    # Plot with fixed aspect ratio
    #plot_circuits(folder, pickle.load(f), aspect_ratio=16/9)
